# rescript-firebase #

>Rescript binding for Firebase

## Table of Contents

* [Installation](#markdown-header-installation)
* [Usage](#markdown-header-usage)
* [Acknowledgements](#markdown-header-acknowledgements)

## Installation

Run the following in your project:
```console
npm install bitbucket:RadiolaRTI/rescript-firebase --save
```

Then, add rescript-firebase in `bsconfig.json`:
```diff
-- "bs-dependencies": [],
++ "bs-dependencies": ["rescript-firebase"],
```

This package includes required peer dependencies and therefore is the only package required to use firebase in your own rescript project
** At least the functions implemented **

## Usage

### Initialize app

```rescript
@val @scope("window")
external firebaseConfig: {..} = "firebaseConfig"

/* unwrap the imported object to get at the actual data */
firebase->initializeApp(firebaseConfig["default"])
```

### Auth

#### Importing

Include the 'firebase/auth' module in our app bundle.

```rescript
let _ = {
  open Firebase.Auth
  require
}
```

#### Watch for user change

```rescript
let (user, setUser) = React.useState(_ => userInitialState)
  React.useEffect0(() => {
    let unsubscribe =
      firebase
      ->auth
      ->Auth.onAuthStateChanged(user => {
        /* set the user to unknown here, because this codepath has been most likely
         triggered by a login/logout attempt; and the `loadUserData` will (re)set it correctly */
        setUser(_ => Unknown)
        switch user->Js.Nullable.toOption {
        | None => setUser(_ => Anonymous)
        | Some(u) => APIAuthFuntions.loadUserData(u, setUser)
        }
      })

    /* clean up the subscription */
    Some(() => unsubscribe)
  })
```

#### Sign in etc

```rescript
    open Js.Promise
    {
      open Firebase
      firebase->auth->Auth.signInWithEmailAndPassword(~email, ~password)
    }
    |> then_(value => {
      Js.log(value)
      Js.Promise.resolve(value)
    })
    |> ignore
```

#### Get token

```rescript
Js.Promise.(
  firebase->auth->Auth.currentUser->Auth.User.getIdToken()
  |> then_(value => {
       Js.log(value);
       Js.Promise.resolve(value);
     })
  |> ignore
);
```

### Firestore

#### Importing

Include the 'firebase/firestore' module in our app bundle.

```rescript
let _ = {
  open Firebase.Firestore
  require
}
```

#### Fetch all

```reason
let fetchAll = () => {
  firebase
    ->firestore
    ->Firestore.collection("mycollection")
    ->Firestore.Collection.get()
    |> Js.Promise.then_(querySnapshot =>
        querySnapshot
        ->Firestore.QuerySnapshot.docs
        ->Belt.Array.map(snapshot => {
            let data = snapshot->Firestore.DocRef.data();
            Js.log(snapshot->Firestore.DocRef.id);
            Js.log(data##someAttribute);

            data;
          })
        |> Js.Promise.resolve
      );
};
```

#### Fetch by id

```reason
let fetchItem = (id) => {
  firebase
  ->firestore
  ->Firestore.collection("mycollection")
  ->Firestore.Collection.doc(id)
  ->Firestore.Collection.DocRef.get()
  |> Js.Promise.then_(doc => {
       let data = doc->Firestore.DocSnapshot.data();
       Js.log(data);

       data;
     });
}
```

#### Create

```reason
let create = (title, description) =>
  firebase
  ->firestore
  ->Firestore.collection("collection")
  ->Firestore.Collection.add({
    "title": title,
    "description": description,
  });
```

#### Update

```reason
let update = (id, title) =>
  firebase
  ->firestore
  ->Firestore.collection("collection")
  ->Firestore.Collection.doc(id)
  ->Firestore.Collection.DocRef.set(
      {"title": title},
      ~options=Firestore.Collection.DocRef.setOptions(~merge=true),
      (),
    );
```

#### Remove

```reason
let remove = (id: string) =>
  firebase
  ->firestore
  ->Firestore.collection("mycollection")
  ->Firestore.Collection.doc(id)
  ->Firestore.Collection.DocRef.delete();
```
## Acknowledgements

@DCKT/Bs-Firebase: https://github.com/DCKT/bs-firebase

@unsignedint/Bs-Firebase: https://github.com/unsignedint/bs-firebase/

Firebase: https://firebase.google.com/